
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import ConnectionPatch
import matplotlib.lines as lines

def draw_corresponding_points(ax1, ax2, coords1, coords2):
    """Dessiner les points correspondants entre deux sets de points sur deux axes différents."""
    for point1, point2 in zip(coords1, coords2):
        con = ConnectionPatch(xyA=point1, xyB=point2, coordsA="data", coordsB="data",
                               axesA=ax1, axesB=ax2, color="blue")
        ax2.add_artist(con)

def plot_epipolar_line(ax, F, x, img_shape):
    """Tracer la droite épipolaire associée à un point donné."""
    l = np.dot(F, x)
    m, n = img_shape
    x_values = [0, n-1]
    y_values = [-(l[2] + l[0] * x_values[0]) / l[1], -(l[2] + l[0] * x_values[1]) / l[1]]
    ax.plot(x_values, y_values, 'g')
    plt.draw()

def eight_point_algorithm(coords_g, coords_d):
    num_points = len(coords_g)
    A = np.zeros((num_points, 9))

    for i in range(num_points):
        xg, yg = coords_g[i]
        xd, yd = coords_d[i]
        A[i] = [xg*xd, yg*xd, xd, xg*yd, yg*yd, yd, xg, yg, 1]

    D = np.dot(A.T, A)
    _, _, V = np.linalg.svd(D)
    F = V[-1].reshape(3, 3)

    # Force F à être une matrice de rang 2
    U, S, V = np.linalg.svd(F)
    S[2] = 0
    F = np.dot(U, np.dot(np.diag(S), V))

    return F

def normalize_points(points):
    """Normaliser les points pour améliorer la précision de l'estimation de F."""
    mean_x = np.mean(points[:, 0])
    mean_y = np.mean(points[:, 1])
    mean_distance = np.mean(np.sqrt((points[:, 0] - mean_x)**2 + (points[:, 1] - mean_y)**2))
    scale = np.sqrt(2) / mean_distance
    T = np.array([
        [scale, 0, -scale * mean_x],
        [0, scale, -scale * mean_y],
        [0, 0, 1]
    ])
    normalized_points = np.dot(T, np.vstack([points.T, np.ones(points.shape[0])])).T[:, :2]
    return T, normalized_points


def finalize():
    global coords_d, coords_g, F
    # Calcul de F
    num_points = len(coords_d)
    T_d, normalized_coords_d = normalize_points(np.array(coords_d))
    T_g, normalized_coords_g = normalize_points(np.array(coords_g))

    F_normalized = eight_point_algorithm(normalized_coords_g, normalized_coords_d)
    F = np.dot(T_g.T, np.dot(F_normalized, T_d))

    print("Matrice F:", F)

    # Calcul des disparités
    disparities = []
    for i in range(len(coords_d)):
        x_d, y_d = coords_d[i]
        x_g, y_g = coords_g[i]
        disparity = abs(x_d - x_g)
        disparities.append(disparity)
    print("Disparités:", disparities)

    xlim0, ylim0 = ax[0].get_xlim(), ax[0].get_ylim()
    xlim1, ylim1 = ax[1].get_xlim(), ax[1].get_ylim()

    # Tracer des lignes reliant les points correspondants entre les deux images
    draw_corresponding_points(ax[0], ax[1], coords_d, coords_g)

    # Tracer les droites épipolaires sur l'image de droite
    for point in coords_g:
        plot_epipolar_line(ax[1], F, [point[0], point[1], 1], np.array(imageD).shape[:2])

    #fixer les limites afin que l'image ne soit pas déformée
    ax[0].set_xlim(xlim0)
    ax[0].set_ylim(ylim0)
    ax[1].set_xlim(xlim1)
    ax[1].set_ylim(ylim1)
    
    plt.draw()

# Charger les images
imageD = Image.open("TurtleD.tif")
imageG = Image.open("TurtleG.tif")

# Initialisation
nombre_de_points = 16
n = 0
coords_d = []
coords_g = []
click_on_d = True

fig, ax = plt.subplots(1, 2)
ax[0].imshow(np.array(imageD))
ax[1].imshow(np.array(imageG))

def plot_new_epipolar_line(event):
    global F
    x, y = int(event.xdata), int(event.ydata)
    coords_g.append((x, y))
    ax[0].scatter([x], [y], c='red')
    plot_epipolar_line(ax[1], F.T, [x, y, 1], np.array(imageD).shape[:2])  # Utilisation de F.T
    plt.draw()

def onclick(event):
    global n, click_on_d, F
    if n < nombre_de_points:
        x, y = int(event.xdata), int(event.ydata)

        if event.inaxes == ax[0] and click_on_d:
            coords_d.append((x, y))
            ax[0].scatter([x], [y], c='red')
            click_on_d = False
        elif event.inaxes == ax[1] and not click_on_d:
            coords_g.append((x, y))
            ax[1].scatter([x], [y], c='blue')
            n += 1
            click_on_d = True
        
        if n == nombre_de_points:
            finalize()
            fig.canvas.mpl_disconnect(cid)
            fig.canvas.mpl_connect('button_press_event', plot_new_epipolar_line)
        plt.draw()
    else:
        plot_new_epipolar_line(event)

cid = fig.canvas.mpl_connect('button_press_event', onclick)
plt.show()